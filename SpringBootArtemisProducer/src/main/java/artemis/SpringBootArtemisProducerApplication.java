package artemis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootArtemisProducerApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(SpringBootArtemisProducerApplication.class, args);
	}
}
