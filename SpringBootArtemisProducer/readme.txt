- download artemis
https://activemq.apache.org/artemis/download.html

- create artemis broker: 
./artemis create /tmp/artemis_broker/

- run artemis
"/private/tmp/artemis_broker/bin/artemis" run

- produce a message
http://localhost:8080/produce?msg=JSA - SpringBoot Artemis, Hello World!
